/**
 * Functions which handles locale storage getter and setter.
 * 
 */
export const getStorage = key => {
    return JSON.parse(localStorage.getItem(key))
}

export const setStorage = (key, value) => {
    localStorage.setItem(key, value)
}