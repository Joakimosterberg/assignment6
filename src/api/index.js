const BASE_URL = 'https://dbassignment6lostintranslation.herokuapp.com/';

/**
 * Checks if the db contains the username, if it does the user is logged in,
 * if not createNewUser is called.
 * @param {String} username 
 */
export const setUsername = async username => {
    const response = await fetch(BASE_URL + "users");
    const data = await response.json()
    let loggedIn = false;
    data.forEach(element => {
        if (element.username.toLowerCase() === username.toLowerCase()) {
            loggedIn = true;
        }
    })
    if (!loggedIn) {
        createNewUser(username);
    }
}
/**
 * Posts a new translation to the db.
 * @param {String} translation 
 */
export const postTranslation = async translation => {
    await fetch(BASE_URL + "translations", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            user: localStorage.getItem('userName'),
            translation: translation,
            isActive: true,
        })
    })
}

/**
 * Fetches all the translations in the db. Loops through them in reverse to get the 10 latest translations. 
 * @returns the 10 latest translations that are set to active.
 */
export const getLatestTranslations = async () => {
    const response = await fetch(BASE_URL + "translations");
    const data = await response.json()
    let retArr = [];
    let count = 0;
    data.slice().reverse().forEach(element => {
        if (element.user === localStorage.getItem('userName') && element.isActive && count < 10) {
            retArr.push(element);
            count++;
        }
    })
    return retArr;
}

/**
 * Fetches all translations then loops through them and looks for a username that matches username in 
 * local storage and that is set to active. These are pushed to idArr.
 * The loop throught idArr uses the id stored to patch the translation matching the id with: isActice: false.
 */
export const clearTranslation = async (id) => {
    await fetch(BASE_URL + "translations/" + id, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            isActive: false
        })
    })
    /*const response = await fetch(BASE_URL+"translations");
    const data = await response.json()
    let idArr = [];
    data.forEach(element => {
        if(element.user === localStorage.getItem('userName') && element.isActive){
            idArr.push(element.id);
        }
    })
    idArr.forEach( async id => {
        */

}

/**
 * Posts a new user to the db with auto generated id.
 * @param {String} username 
 */
const createNewUser = async username => {
    await fetch(BASE_URL + "users", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username: username })
    })
}
