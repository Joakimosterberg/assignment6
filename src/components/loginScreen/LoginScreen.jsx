import { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { setStorage } from "../../storage/index";
import { setUsername } from "../../api/index";
import "./LoginScreen.css";

const LoginScreen = () => {

    const history = useHistory()

    const [loginState, setLoginState] = useState('')

    /**
     * Const to fetch userName from localstorage. Later used to validate if there's an active user.
     */
    const isLoggedIn = localStorage.getItem('userName')

    /**
     * Binding input on inputchange from the user and setting loginstate. 
     * @param {*} event 
     */
    const onInputChange = event => {
        setLoginState(
            event.target.value
        )
    }

    /**
     * Function which login the user and pushing to translationscreen. 
     * Error handling to validate input from user, which should not be empty.
     * Else, using localstorage to set username.
     */
    const login = () => {
        if (loginState === '') {
            alert("Enter a username")
        } else {
            setStorage('userName', loginState)
            setUsername(loginState);
            history.push("/translationScreen");
        }
    }

    return (
        <div className="login-screen">

            {/* If theres an active user for the application, redirect to the translationScreen instead of loginScreen. */}
            {isLoggedIn && <Redirect to="/translationScreen" />}

            <ul className="nav d-flex justify-content between">
                <li className="nav-item">
                    <span className="navbar-text">Lost in translation</span>
                </li>
            </ul>

            <div className="login-container">
                <div className="welcome-section">
                    <div className="row">
                        <div className="col">
                            <h2>Welcome to Lost in translation</h2>
                            <h4>Get started</h4>
                        </div>
                    </div>

                </div>
                <div className="loginInput">
                    <input type="text"
                        id="userName"
                        placeholder="Enter your username"
                        className="form-control"
                        onChange={onInputChange}
                    />
                    <button className="btn btn-primary" onClick={login}>Login</button>
                </div>
            </div>
        </div>
    )
}

export default LoginScreen;