import { useEffect, useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { getLatestTranslations, clearTranslation } from "../../api";
import "./ProfileScreen.css";

const ProfileScreen = () => {

    const history = useHistory()

    const [username, setUserName] = useState('')

    /**
     * Const to fetch userName from localstorage. Later used to validate if there's an active user.
     */
    const isLoggedIn = localStorage.getItem('userName')

    const [translations, setTranslations] = useState([]);

    const goToTranslationScreen = () => {
        history.push('/translationScreen')
    }

    /**
     * Function which clears all the translations. This interacts with onClick button from the user.
     */
    const handleClearTranslation = (id) => {
        clearTranslation(id);
        setTranslations(translations.filter(item => item.id !== id))
    }

    /**
     * Function which log outs the current user, which interacts with onClick from user.
     * Clearing localStorage and username, then pusing to loginScreen.
     */
    const handleLogout = () => {
        localStorage.clear();
        setUserName('')
        history.push("/");
    }

    useEffect(() => {
        setUserName(localStorage.getItem('userName'));
        (async () => {
            const result = await getLatestTranslations()
            setTranslations(result)
        })()
    }, [])


    return (
        <div className="profile-screen">

            {/* If theres not an active user for the application, redirect to the loginScreen. This means that profileScreen is guarded from inactive users.*/}
            {!isLoggedIn && <Redirect to="/" />}

            <ul className="nav">
                <li className="nav-item">
                    <span className="navbar-text">Lost in translation </span>
                </li>
                <li className="nav-item">
                    <i class="fa fa-fw fa-user">{username}</i>
                </li>
                <li className="nav-item">
                    <button onClick={goToTranslationScreen} className="btn btn-primary">Translation page</button>
                    <button onClick={handleLogout} className="btn btn-primary">Logout user</button>
                </li>

            </ul>

            <div className="translation-history">
                <h1>Your latest translations:</h1>
                <ul>
                    {translations.map(item => (
                        <li className="translation-item" key={item.id}>
                            {item.translation}
                            <button className="btn btn-danger" onClick={() => handleClearTranslation(item.id)}>Remove</button>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default ProfileScreen;