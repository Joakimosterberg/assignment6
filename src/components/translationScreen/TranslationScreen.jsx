import { useEffect, useRef, useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { setStorage } from "../../storage/index";
import { postTranslation } from "../../api";
import "./TranslationScreen.css";

const TranslationScreen = () => {

    const history = useHistory()

    /**
     * Const to fetch userName from localstorage. Later used to validate if there's an active user.
     */
    const isLoggedIn = localStorage.getItem('userName')

    const refInputTranslation = useRef()

    const [ translationState, setTranslationState ] = useState({
        translationPhrase: ''
    })

    const [image, setImage] = useState([])

    const [username, setUserName] = useState('')

    useEffect(() => {
        setUserName(localStorage.getItem('userName'))
    }, [])

    /**
     * Binding input on inputchange from the user and setting translationstate.
     * @param {*} event 
     */
    const onInputChange = event => {
        setTranslationState(
            event.target.value
        )
    }

    const goToProfileScreen = () => {
        history.push('/profileScreen')
    }

    /**
     * Function for translating a given word from input and displays matching images from sign language.
     * Using local storage to set translationState and then puts the value into a new array.
     * Setting the word to lowercase, also split numbers from input.
     * Through map method, displays one image for each index and display it. 
     * Lastly, posting translation to db.
     */
    const translateWord = () => {
        if (translationState.translationPhrase === '') {
            alert("Enter a phrase you wish to translate")
        } else {
            setStorage('phrase', translationState)
            const translationArray = [];

            for (let i = 0; i < translationState.length; i++) {
                translationArray.push(translationState[i].toLowerCase().split(/[ 0-9,.]$/).join(''))
            }
            setImage(translationArray.map((image, index) => {
                return <img className="img-thumbnail"
                    key={index}
                    src={`/individial_signs/${image}.png`}
                    alt={image} />
            }))
            postTranslation(translationState);
            refInputTranslation.current.value = ""
        }
    }
    return (
        <div className="translation-screen">

            {/* If theres not an active user for the application, redirect to the loginScreen. This means that profileScreen is guarded from inactive users.*/}
            {!isLoggedIn && <Redirect to="/" />}

            <ul className="nav">
                <li className="nav-item">
                    <span className="navbar-text">Lost in translation </span>
                </li>
                <li className="nav-item">
                    <i class="fa fa-fw fa-user">{username}</i>
                </li>
                <li className="nav-item">
                    <button onClick={goToProfileScreen} className="btn btn-primary">Profile</button>
                </li>
            </ul>
            <div className="translation-container">
                <div className="input">
                    <h3>To translate:</h3>
                    <input
                        className="form-control"
                        id="translationPhrase"
                        placeholder="Enter a word"
                        onChange={onInputChange}
                        ref={refInputTranslation}
                    />
                    <button className="btn btn-primary" onClick={translateWord}>Translate</button>
                </div>
                <div className="output">
                    <h3>Translation:</h3>
                    <div className="card-body">
                        <div>
                            {image}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TranslationScreen;