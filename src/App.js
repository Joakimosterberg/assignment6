import './App.css';
import React from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import LoginScreen from './components/loginScreen/LoginScreen';
import TranslationScreen from './components//translationScreen/TranslationScreen';
import ProfileScreen from './components/profileScreen/ProfileScreen';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/profileScreen">
            <ProfileScreen />
          </Route>
          <Route path="/translationScreen">
            <TranslationScreen />
          </Route>
          <Route path="/">
            <LoginScreen />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}


export default App;
