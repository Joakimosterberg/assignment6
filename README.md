# Assignment

This is Assignment is called Lost in Translation, where the task was to build an online language translator as a SPA using the React framework. The application will transfer an input from the user, a word, and then translate it to sign language images which displays on the website. For the assignment the application offers the user to log in with a username, and later store the latest translated words for that specific user. 

# Project structure

Three main components has been used in this assignment. A login screen where the user can enter the username, a translate screen where the user can type in a word in a field, and later visualize the translated word in sign language, and lastly a profile screen where the user can see the 10 latest translations, clear the translation list and also logout from the application. A node.JS database has been used as backend, where the user's name and translations has been stored. Localstorage has been utilized to handle state from the different components. 

# Heroku-link
Deployed Heroku app: [Lost in Translation](https://assignment6experisjoakimpontus.herokuapp.com/)

# Figma-link for the projet
[Figma-link](https://gitlab.com/Joakimosterberg/assignment6/-/blob/master/Assignment6%20Figma%20sketches.pdf)    

## Members
**Joakim Österberg & Pontus Rohdén** 

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
